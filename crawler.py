#!/usr/bin/env python

from urllib.parse import urljoin
from lxml import html
import click
import requests
import re


def parse(html_content, current_url):
    """
    Parser. Extracting emails and URLs on page.
    :param html_content: (str) html page
    :param current_url: (str) current URL
    :return: (list) two list - parsed urls and emails
    """
    # parsing emails
    parsed_emails = re.findall(
        '([a-zA-Z0-9._\-]+@[a-zA-Z0-9._\-]+\.[a-zA-Z0-9]+)', html_content)
    parsed_emails = set([e.lower() for e in parsed_emails])
    # parsing urls
    parsed_body = html.fromstring(html_content)
    urls = parsed_body.xpath('//*/a/@href')
    parsed_urls = [urljoin(current_url, u) for u in urls if not u.startswith('mailto:')]
    return parsed_urls, parsed_emails

def spider(urls):
    """
    Spider. Goes to URLs for parse page.
    :param urls: (list) target URL list
    :return: parsed (list) parsed URL list
    """
    next_urls = []
    for page_urls in urls:
        for url in page_urls:
            # get request
            response = requests.get(url=url)
            # page parsing
            list_urls, list_emails = parse(response.text, response.url)
            next_urls.append(list_urls)
            print('----------------------------')
            print('URL {}'.format(url))
            print('----------------------------')
            for i in list_emails:
                print(i)
    return next_urls

# Command Line Interface
@click.command()
@click.argument('start_url', required=True)
@click.option('--deep', default=1, required=False,
              help='How deep should it parse pages, Default=1')
def main(start_url, deep):

    urls = [[start_url]]
    # run spider with specified deepness
    for n in range(deep):
        urls = spider(urls)
    print('========= Finished! ==========')

if __name__ == '__main__':
    main()
